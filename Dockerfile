FROM python

WORKDIR /app

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

CMD hypercorn api:app --bind 0.0.0.0:80 --reload