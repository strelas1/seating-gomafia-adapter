import fastapi
from bs4 import BeautifulSoup
import requests
from fake_headers import Headers

app = fastapi.FastAPI()


async def get_headers():
    headers = Headers(browser='chrome', os='win')
    return headers.generate()


async def get_request(url):
    headers = await get_headers()
    response = requests.get(url, headers=headers)
    return response.text


@app.get("/gomafia_seating")
async def get_gomafia_seating(id: int):
    url = f"https://gomafia.pro/tournament/{id}?tab=games"
    response = await get_request(url)
    soup = BeautifulSoup(response, 'html.parser')
    players_count = soup.find_all("div",
                                  class_='_tid__tournament__top-left-item-description__nFBnP')[-1].text.split()[0]
    nicknames_list = soup.find_all('div', class_='d-flex')[1:]
    result_list = await format_data(nicknames_list)
    return result_list


async def format_data(nicknames: list):
    result = []
    current_tour = []
    game = {}
    players = []
    for nickname in nicknames:
        if 'Стол ' in nickname.text:
            table_split = nickname.text.split(', ')
            referee = table_split[-1]
            current_table = table_split[0].split(' ')[-1]

            if current_table == "1":
                if (len(current_tour) > 0):
                    result.append(current_tour)
                    current_tour = []

            if players:
                game['players'] = players

            game['referee'] = referee
        else:
            players.append(nickname.text)

        if 'referee' in game and len(players) == 10:
            game['players'] = players
            current_tour.append(game)
            game = {}
            players = []
    if current_tour:
        result.append(current_tour)

    return result





